### Getting Started

Have [Node.js](https://nodejs.org/) and npm installed.

In the working directory terminal/command prompt you have this project:

`cd path/to/workspace/navomi-tasks`

`npm i` 

### Development server

Type `npm start` then Navigate to [`http://localhost:3000`](http://localhost:3000). 
