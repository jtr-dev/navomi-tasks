var socket = io.connect();

var login = new Vue({
    el: '#header',
    data: {
        loggedIn: false,
        profile: {}
    },
    methods: {}
})

var vm = new Vue({
    el: '#app',
    data: {
        loggedIn: false,
        score: 0,
        entities: [],
        entity: '',
        sentence: '',
        history: [{ user: '' }, { bot: '' }],
        sentiment: '',
        entityPhoto: ''
    },
    methods: {
        parseNL: (sentence) => {
            if (!vm.loggedIn) { return false }
            vm.history.push({ user: sentence })
            vm.sentence = ''
            socket.emit('parseNL', sentence)
        },
        wikiLink: (entity) => {
            return `https://en.wikipedia.org/wiki/${entity}`
        },
        entityImg: (entity) => {
            var xhr = $.get(`http://api.giphy.com/v1/gifs/search?q=${entity}&api_key=dc6zaTOxFJmzC&limit=1`);
            xhr.done(function (data) {
                vm.entityPhoto = data.data[0].images.downsized_still.url + '.png'
            });
        }
    }
})

const getResponse = () => {
    const responses = {
        "Positive": [
            "That's awesome to hear!",
            "Nice!"
        ],
        "Neutral": [
            `Interesting, I'd like to hear more about ${vm.entity || 'that'}`,
            `I like ${vm.entity || 'that'} too!`
        ],
        "Negative": [
            "Hmm, why don't we talk about nicer things",
            "Yknow, I really like playing guitar"
        ]
    }
    let query = responses[vm.sentiment]
    vm.history.push({ bot: query[Math.floor(Math.random() * query.length)] })
}

const handleEntities = (res) => {
    let ntts = res.entities[0].entities
    vm.entities = [];
    ntts.map(n => vm.entities.push({ name: n.name, type: n.type }))

    if (vm.entities.length >= 1){
        vm.entity = ntts[0].name
        vm.entityImg(vm.entity)
    }
    
}

const handleScore = (res) => {
    const senti = res.sentiment[0]
    const score = senti.documentSentiment.score;

    (score > 0.3)
        ? vm.sentiment = "Positive"
        : (score < 0.3 && score > 0.1)
            ? vm.sentiment = "Neutral"
            : vm.sentiment = "Negative"

    vm.score = score

    getResponse()
}

const componentUpdate = (res) => {
    if (res.entities) {
        handleEntities(res)
    } else if (res.syntax) {
        // possibly check for nouns, adverbs here...
    } else if (res.sentiment) {
        handleScore(res)
    }
}

socket.on('parsedNL', (res) => {
    componentUpdate(res)
})

socket.on('login', data => {
    vm.loggedIn = true;
    login.loggedIn = true;
    login.profile = { email: data.user.google.email, photo: data.user.google.photos[0].value }
})

socket.on('logout', () => {
    vm.loggedIn = false;
    login.loggedIn = false;
})